#!/usr/bin/env bash

# Install test dependencies
apt-get update
apt-get -y install gcc make --no-install-recommends # Needed to compile native extensions

cd /usr/src/redmine

# Copy the plugin to the plugins folder
if [ -n "$BITBUCKET_CLONE_DIR" ]; then
	mkdir plugins/bitbucket_reference_redmine
	cp -a $BITBUCKET_CLONE_DIR/* plugins/bitbucket_reference_redmine;
	cp ./plugins/bitbucket_reference_redmine/test-database.yml ./config/database.yml;
else
	echo "Assuming plugin was installed in plugins folder";
fi

bundle install --without development rmagick --with test

export RAILS_ENV=test
rake db:migrate redmine:plugins:migrate redmine:plugins:test
